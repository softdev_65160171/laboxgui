/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.oxoop;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


/**
 *
 * @author brzho
 */

public class TestWritePlayer {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Player x = new Player('X');
        Player o = new Player('O');
        o.draw();
        x.draw();
        o.loss();
        o.loss();
        x.win();
        x.win();
        System.out.println(x);
        System.out.println(o);
        File file = new File("Player.dat");
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(o);
        oos.writeObject(x);
        oos.close();
        fos.close();
    }
}
